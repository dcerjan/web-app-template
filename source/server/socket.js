"use strict";

module.exports = (server) => {
  let io = require("socket.io")(server);

  io.on("connection", (socket) => {
    socket.emit("connected", { status: "ok", message: "Wellcome traveler!" });

    socket.on("message", (message) => {
      io.emit("message", {timestamp: Date.now(), message});
    });
  });
};
