"use strict";

module.exports = (app) => {

  app.get(["/dev"], (req, res) => {
    res.render("dev.ejs");
  });

  app.get(["/", "index"], (req, res) => {
    res.render("index.ejs");
  });

};
