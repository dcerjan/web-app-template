import React from "react";
import ReactDOM from "react-dom";

import Chat from "./chat/Chat";

ReactDOM.render(<Chat/>, window.document.getElementById("main"));
