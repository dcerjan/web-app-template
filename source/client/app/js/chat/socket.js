import io from "socket.io";

let socket = io.connect(":8080");

export default socket;
