import React from "react";
import MessageStore from "./messageStore";

class Chat extends React.Component {
    constructor(props) {
        super(props);

        MessageStore.subscribe((message) => {
            this.setState({messages: this.state.messages.concat(message)});
        });

        this.state = {
            messages: [],
            message: ""
        };
    }

    componentWillUpdate() {
        let node = this.refs.msgContainer;
        this.shouldScrollBottom = node.scrollTop + node.offsetHeight - node.scrollHeight >= 0;
    }

    componentDidUpdate() {
        if (this.shouldScrollBottom) {
            let node = this.refs.msgContainer;
            node.scrollTop = node.scrollHeight;
        }
    }

    render() {
        return (
            <div className="chat">
                <div ref="msgContainer">{
                    this.state.messages.map((msg, index) => {
                        let time = new Date(msg.timestamp);

                        let hours = time.getHours();
                        hours = hours < 10 ? "0" + hours : hours;
                        let minutes = time.getMinutes();
                        minutes = minutes < 10 ? "0" + minutes : minutes;
                        let seconds = time.getSeconds();
                        seconds = seconds < 10 ? "0" + seconds : seconds;

                        return <p key={index}>{`${hours}:${minutes}:${seconds} > ${msg.message}`}</p>;
                    })
                }</div>
                <div onKeyPress={(event) => {
                    if (event.charCode === 13) {
                        MessageStore.publish(this.state.message);
                        this.setState({message: ""});
                    }
                }}>
                    <input onChange={(event) => {
                        this.setState({message: event.target.value});
                    }} value={this.state.message}/>
                    <div onClick={() => {
                        MessageStore.publish(this.state.message);
                        this.setState({message: ""});
                    }}>Send</div>
                </div>
            </div>
        );
    }
}

export default Chat;
